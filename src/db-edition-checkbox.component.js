/**
 * Created by richard on 19/10/16.
 */

/*global angular */
'use strict';

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-db-edition-checkbox')
    .component('bitcraftDbEditionCheckbox', {
        templateUrl: './js/db-edition-checkbox/db-edition-checkbox.template.html', // this line will be replaced
        bindings: {
            value: '=',
            edit: '<'
        }
    });
